﻿namespace Main
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_0 = new System.Windows.Forms.Button();
            this.button_2 = new System.Windows.Forms.Button();
            this.button_5 = new System.Windows.Forms.Button();
            this.button_8 = new System.Windows.Forms.Button();
            this.button_7 = new System.Windows.Forms.Button();
            this.button_3 = new System.Windows.Forms.Button();
            this.button_4 = new System.Windows.Forms.Button();
            this.button_9 = new System.Windows.Forms.Button();
            this.button_1 = new System.Windows.Forms.Button();
            this.button_6 = new System.Windows.Forms.Button();
            this.button_B = new System.Windows.Forms.Button();
            this.button_E = new System.Windows.Forms.Button();
            this.button_C = new System.Windows.Forms.Button();
            this.button_F = new System.Windows.Forms.Button();
            this.button_D = new System.Windows.Forms.Button();
            this.button_A = new System.Windows.Forms.Button();
            this.radioButton_hex = new System.Windows.Forms.RadioButton();
            this.radioButton_dec = new System.Windows.Forms.RadioButton();
            this.radioButton_oct = new System.Windows.Forms.RadioButton();
            this.radioButton_bin = new System.Windows.Forms.RadioButton();
            this.label_hex = new System.Windows.Forms.Label();
            this.label_dec = new System.Windows.Forms.Label();
            this.label_oct = new System.Windows.Forms.Label();
            this.label_bin = new System.Windows.Forms.Label();
            this.button_plus = new System.Windows.Forms.Button();
            this.button_min = new System.Windows.Forms.Button();
            this.button_div = new System.Windows.Forms.Button();
            this.button_mul = new System.Windows.Forms.Button();
            this.button_dot = new System.Windows.Forms.Button();
            this.button_equal = new System.Windows.Forms.Button();
            this.content = new System.Windows.Forms.CheckBox();
            this.button_ms = new System.Windows.Forms.Button();
            this.button_mmin = new System.Windows.Forms.Button();
            this.button_mc = new System.Windows.Forms.Button();
            this.button_mpl = new System.Windows.Forms.Button();
            this.content2 = new System.Windows.Forms.CheckBox();
            this.listBox_mem = new System.Windows.Forms.ListBox();
            this.AC = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_0
            // 
            this.button_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_0, "button_0");
            this.button_0.Name = "button_0";
            this.button_0.UseVisualStyleBackColor = false;
            this.button_0.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_2
            // 
            this.button_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_2, "button_2");
            this.button_2.Name = "button_2";
            this.button_2.UseVisualStyleBackColor = false;
            this.button_2.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_5
            // 
            this.button_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_5, "button_5");
            this.button_5.Name = "button_5";
            this.button_5.UseVisualStyleBackColor = false;
            this.button_5.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_8
            // 
            this.button_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_8, "button_8");
            this.button_8.Name = "button_8";
            this.button_8.UseVisualStyleBackColor = false;
            this.button_8.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_7
            // 
            this.button_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_7, "button_7");
            this.button_7.Name = "button_7";
            this.button_7.UseVisualStyleBackColor = false;
            this.button_7.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_3
            // 
            this.button_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_3, "button_3");
            this.button_3.Name = "button_3";
            this.button_3.UseVisualStyleBackColor = false;
            this.button_3.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_4
            // 
            this.button_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_4, "button_4");
            this.button_4.Name = "button_4";
            this.button_4.UseVisualStyleBackColor = false;
            this.button_4.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_9
            // 
            this.button_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_9, "button_9");
            this.button_9.Name = "button_9";
            this.button_9.UseVisualStyleBackColor = false;
            this.button_9.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_1
            // 
            this.button_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_1, "button_1");
            this.button_1.Name = "button_1";
            this.button_1.UseVisualStyleBackColor = false;
            this.button_1.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_6
            // 
            this.button_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_6, "button_6");
            this.button_6.Name = "button_6";
            this.button_6.UseVisualStyleBackColor = false;
            this.button_6.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_B
            // 
            this.button_B.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_B.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_B.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_B, "button_B");
            this.button_B.Name = "button_B";
            this.button_B.UseVisualStyleBackColor = false;
            this.button_B.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_E
            // 
            this.button_E.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_E.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_E.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_E, "button_E");
            this.button_E.Name = "button_E";
            this.button_E.UseVisualStyleBackColor = false;
            this.button_E.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_C
            // 
            this.button_C.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_C.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_C.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_C, "button_C");
            this.button_C.Name = "button_C";
            this.button_C.UseVisualStyleBackColor = false;
            this.button_C.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_F
            // 
            this.button_F.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_F.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_F.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_F, "button_F");
            this.button_F.Name = "button_F";
            this.button_F.UseVisualStyleBackColor = false;
            this.button_F.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_D
            // 
            this.button_D.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_D.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_D.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_D, "button_D");
            this.button_D.Name = "button_D";
            this.button_D.UseVisualStyleBackColor = false;
            this.button_D.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_A
            // 
            this.button_A.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_A.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_A.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_A, "button_A");
            this.button_A.Name = "button_A";
            this.button_A.UseVisualStyleBackColor = false;
            this.button_A.Click += new System.EventHandler(this.button_A_Click);
            // 
            // radioButton_hex
            // 
            resources.ApplyResources(this.radioButton_hex, "radioButton_hex");
            this.radioButton_hex.BackColor = System.Drawing.Color.Black;
            this.radioButton_hex.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton_hex.FlatAppearance.BorderSize = 0;
            this.radioButton_hex.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_hex.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.radioButton_hex.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.radioButton_hex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_hex.Name = "radioButton_hex";
            this.radioButton_hex.UseVisualStyleBackColor = false;
            this.radioButton_hex.CheckedChanged += new System.EventHandler(this.radioButton_hex_CheckedChanged);
            // 
            // radioButton_dec
            // 
            resources.ApplyResources(this.radioButton_dec, "radioButton_dec");
            this.radioButton_dec.BackColor = System.Drawing.Color.Black;
            this.radioButton_dec.Checked = true;
            this.radioButton_dec.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton_dec.FlatAppearance.BorderSize = 0;
            this.radioButton_dec.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_dec.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.radioButton_dec.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.radioButton_dec.Name = "radioButton_dec";
            this.radioButton_dec.TabStop = true;
            this.radioButton_dec.UseVisualStyleBackColor = false;
            this.radioButton_dec.CheckedChanged += new System.EventHandler(this.radioButton_dec_CheckedChanged);
            // 
            // radioButton_oct
            // 
            resources.ApplyResources(this.radioButton_oct, "radioButton_oct");
            this.radioButton_oct.BackColor = System.Drawing.Color.Black;
            this.radioButton_oct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton_oct.FlatAppearance.BorderSize = 0;
            this.radioButton_oct.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_oct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.radioButton_oct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.radioButton_oct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_oct.Name = "radioButton_oct";
            this.radioButton_oct.UseVisualStyleBackColor = false;
            this.radioButton_oct.CheckedChanged += new System.EventHandler(this.radioButton_oct_CheckedChanged);
            // 
            // radioButton_bin
            // 
            resources.ApplyResources(this.radioButton_bin, "radioButton_bin");
            this.radioButton_bin.BackColor = System.Drawing.Color.Black;
            this.radioButton_bin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButton_bin.FlatAppearance.BorderSize = 0;
            this.radioButton_bin.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_bin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.radioButton_bin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.radioButton_bin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton_bin.Name = "radioButton_bin";
            this.radioButton_bin.UseVisualStyleBackColor = false;
            this.radioButton_bin.CheckedChanged += new System.EventHandler(this.radioButton_bin_CheckedChanged);
            // 
            // label_hex
            // 
            resources.ApplyResources(this.label_hex, "label_hex");
            this.label_hex.BackColor = System.Drawing.Color.Black;
            this.label_hex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label_hex.Name = "label_hex";
            // 
            // label_dec
            // 
            resources.ApplyResources(this.label_dec, "label_dec");
            this.label_dec.BackColor = System.Drawing.Color.Black;
            this.label_dec.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label_dec.Name = "label_dec";
            // 
            // label_oct
            // 
            resources.ApplyResources(this.label_oct, "label_oct");
            this.label_oct.BackColor = System.Drawing.Color.Black;
            this.label_oct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label_oct.Name = "label_oct";
            // 
            // label_bin
            // 
            resources.ApplyResources(this.label_bin, "label_bin");
            this.label_bin.BackColor = System.Drawing.Color.Black;
            this.label_bin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label_bin.Name = "label_bin";
            // 
            // button_plus
            // 
            this.button_plus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_plus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_plus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_plus, "button_plus");
            this.button_plus.Name = "button_plus";
            this.button_plus.UseVisualStyleBackColor = false;
            this.button_plus.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_min
            // 
            this.button_min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_min.FlatAppearance.BorderSize = 0;
            this.button_min.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_min.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_min, "button_min");
            this.button_min.Name = "button_min";
            this.button_min.UseVisualStyleBackColor = false;
            this.button_min.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_div
            // 
            this.button_div.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_div.FlatAppearance.BorderSize = 0;
            this.button_div.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_div.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_div, "button_div");
            this.button_div.Name = "button_div";
            this.button_div.UseVisualStyleBackColor = false;
            this.button_div.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_mul
            // 
            this.button_mul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mul.FlatAppearance.BorderSize = 0;
            this.button_mul.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mul.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_mul, "button_mul");
            this.button_mul.Name = "button_mul";
            this.button_mul.UseVisualStyleBackColor = false;
            this.button_mul.Click += new System.EventHandler(this.button_A_Click);
            // 
            // button_dot
            // 
            this.button_dot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_dot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_dot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_dot, "button_dot");
            this.button_dot.Name = "button_dot";
            this.button_dot.UseVisualStyleBackColor = false;
            this.button_dot.Click += new System.EventHandler(this.button_dot_Click);
            // 
            // button_equal
            // 
            this.button_equal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_equal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_equal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_equal, "button_equal");
            this.button_equal.Name = "button_equal";
            this.button_equal.UseVisualStyleBackColor = false;
            this.button_equal.Click += new System.EventHandler(this.button_equal_Click);
            // 
            // content
            // 
            resources.ApplyResources(this.content, "content");
            this.content.AutoCheck = false;
            this.content.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.content.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.content.FlatAppearance.BorderSize = 0;
            this.content.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.content.Name = "content";
            this.content.UseVisualStyleBackColor = false;
            this.content.CheckedChanged += new System.EventHandler(this.content2_CheckedChanged);
            this.content.Click += new System.EventHandler(this.content_Click);
            // 
            // button_ms
            // 
            this.button_ms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_ms.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_ms.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_ms, "button_ms");
            this.button_ms.Name = "button_ms";
            this.button_ms.UseVisualStyleBackColor = false;
            this.button_ms.Click += new System.EventHandler(this.button_ms_Click);
            // 
            // button_mmin
            // 
            this.button_mmin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_mmin, "button_mmin");
            this.button_mmin.Name = "button_mmin";
            this.button_mmin.UseVisualStyleBackColor = false;
            this.button_mmin.Click += new System.EventHandler(this.button_ms_Click);
            // 
            // button_mc
            // 
            this.button_mc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_mc, "button_mc");
            this.button_mc.Name = "button_mc";
            this.button_mc.UseVisualStyleBackColor = false;
            this.button_mc.Click += new System.EventHandler(this.button_ms_Click);
            // 
            // button_mpl
            // 
            this.button_mpl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mpl.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_mpl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.button_mpl, "button_mpl");
            this.button_mpl.Name = "button_mpl";
            this.button_mpl.UseVisualStyleBackColor = false;
            this.button_mpl.Click += new System.EventHandler(this.button_ms_Click);
            // 
            // content2
            // 
            resources.ApplyResources(this.content2, "content2");
            this.content2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.content2.FlatAppearance.BorderSize = 0;
            this.content2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.content2.Name = "content2";
            this.content2.UseVisualStyleBackColor = false;
            this.content2.CheckedChanged += new System.EventHandler(this.content2_CheckedChanged);
            this.content2.Click += new System.EventHandler(this.content2_Click);
            // 
            // listBox_mem
            // 
            this.listBox_mem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.listBox_mem.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.listBox_mem, "listBox_mem");
            this.listBox_mem.FormattingEnabled = true;
            this.listBox_mem.Name = "listBox_mem";
            this.listBox_mem.DoubleClick += new System.EventHandler(this.listBox_mem_DoubleClick);
            // 
            // AC
            // 
            this.AC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.AC, "AC");
            this.AC.Name = "AC";
            this.AC.UseVisualStyleBackColor = false;
            this.AC.Click += new System.EventHandler(this.AC_Click);
            // 
            // Back
            // 
            this.Back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.Back, "Back");
            this.Back.Name = "Back";
            this.Back.UseVisualStyleBackColor = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.Back);
            this.Controls.Add(this.AC);
            this.Controls.Add(this.listBox_mem);
            this.Controls.Add(this.content2);
            this.Controls.Add(this.button_mpl);
            this.Controls.Add(this.button_mc);
            this.Controls.Add(this.button_mmin);
            this.Controls.Add(this.button_ms);
            this.Controls.Add(this.content);
            this.Controls.Add(this.button_equal);
            this.Controls.Add(this.button_dot);
            this.Controls.Add(this.button_mul);
            this.Controls.Add(this.button_div);
            this.Controls.Add(this.button_min);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.label_bin);
            this.Controls.Add(this.label_oct);
            this.Controls.Add(this.label_dec);
            this.Controls.Add(this.label_hex);
            this.Controls.Add(this.radioButton_bin);
            this.Controls.Add(this.radioButton_oct);
            this.Controls.Add(this.radioButton_dec);
            this.Controls.Add(this.radioButton_hex);
            this.Controls.Add(this.button_A);
            this.Controls.Add(this.button_D);
            this.Controls.Add(this.button_F);
            this.Controls.Add(this.button_C);
            this.Controls.Add(this.button_E);
            this.Controls.Add(this.button_B);
            this.Controls.Add(this.button_6);
            this.Controls.Add(this.button_1);
            this.Controls.Add(this.button_9);
            this.Controls.Add(this.button_4);
            this.Controls.Add(this.button_3);
            this.Controls.Add(this.button_7);
            this.Controls.Add(this.button_8);
            this.Controls.Add(this.button_5);
            this.Controls.Add(this.button_2);
            this.Controls.Add(this.button_0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_0;
        private System.Windows.Forms.Button button_2;
        private System.Windows.Forms.Button button_5;
        private System.Windows.Forms.Button button_8;
        private System.Windows.Forms.Button button_7;
        private System.Windows.Forms.Button button_3;
        private System.Windows.Forms.Button button_4;
        private System.Windows.Forms.Button button_9;
        private System.Windows.Forms.Button button_1;
        private System.Windows.Forms.Button button_6;
        private System.Windows.Forms.Button button_B;
        private System.Windows.Forms.Button button_E;
        private System.Windows.Forms.Button button_C;
        private System.Windows.Forms.Button button_F;
        private System.Windows.Forms.Button button_D;
        private System.Windows.Forms.Button button_A;
        private System.Windows.Forms.RadioButton radioButton_hex;
        private System.Windows.Forms.RadioButton radioButton_dec;
        private System.Windows.Forms.RadioButton radioButton_oct;
        private System.Windows.Forms.RadioButton radioButton_bin;
        private System.Windows.Forms.Label label_hex;
        private System.Windows.Forms.Label label_dec;
        private System.Windows.Forms.Label label_oct;
        private System.Windows.Forms.Label label_bin;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button_min;
        private System.Windows.Forms.Button button_div;
        private System.Windows.Forms.Button button_mul;
        private System.Windows.Forms.Button button_dot;
        private System.Windows.Forms.Button button_equal;
        private System.Windows.Forms.CheckBox content;
        private System.Windows.Forms.Button button_ms;
        private System.Windows.Forms.Button button_mmin;
        private System.Windows.Forms.Button button_mc;
        private System.Windows.Forms.Button button_mpl;
        private System.Windows.Forms.CheckBox content2;
        private System.Windows.Forms.ListBox listBox_mem;
        private System.Windows.Forms.Button AC;
        private System.Windows.Forms.Button Back;
    }
}

