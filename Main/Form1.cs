﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;
using System.Text.RegularExpressions;

namespace Main
{
    public  partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        Systems oper1;
        Systems oper2;
        Systems res;
        string operation = null;
        private void radioButton_hex_CheckedChanged(object sender, EventArgs e)
        {
            button_A.Enabled = true;
            button_B.Enabled = true;
            button_C.Enabled = true;
            button_D.Enabled = true;
            button_E.Enabled = true;
            button_F.Enabled = true;
            button_2.Enabled = true;
            button_3.Enabled = true;
            button_4.Enabled = true;
            button_5.Enabled = true;
            button_6.Enabled = true;
            button_7.Enabled = true;
            button_8.Enabled = true;
            button_9.Enabled = true;
            if (content.Checked) content.Text = label_hex.Text;
            if (content2.Checked) content2.Text = label_hex.Text;
            SetRadioColor();
            CheckForDot(label_hex.Text);
            radioButton_hex.ForeColor = Color.Black;
        }

        private void radioButton_dec_CheckedChanged(object sender, EventArgs e)
        {
            button_A.Enabled = false;
            button_B.Enabled = false;
            button_C.Enabled = false;
            button_D.Enabled = false;
            button_E.Enabled = false;
            button_F.Enabled = false;
            button_2.Enabled = true;
            button_3.Enabled = true;
            button_4.Enabled = true;
            button_5.Enabled = true;
            button_6.Enabled = true;
            button_7.Enabled = true;
            button_8.Enabled = true;
            button_9.Enabled = true;
            if(content.Checked) content.Text = label_dec.Text;
            if (content2.Checked) content2.Text = label_dec.Text;
            SetRadioColor();
            CheckForDot(label_dec.Text);
            radioButton_dec.ForeColor = Color.Black;
        }
        private void radioButton_oct_CheckedChanged(object sender, EventArgs e)
        {
            button_A.Enabled = false;
            button_B.Enabled = false;
            button_C.Enabled = false;
            button_D.Enabled = false;
            button_E.Enabled = false;
            button_F.Enabled = false;
            button_2.Enabled = true;
            button_3.Enabled = true;
            button_4.Enabled = true;
            button_5.Enabled = true;
            button_6.Enabled = true;
            button_7.Enabled = true;
            button_8.Enabled = false;
            button_9.Enabled = false;
            if (content.Checked) content.Text = label_oct.Text;
            if (content2.Checked) content2.Text = label_oct.Text;
            SetRadioColor();
            CheckForDot(label_oct.Text);
            radioButton_oct.ForeColor = Color.Black;
        }
        private void radioButton_bin_CheckedChanged(object sender, EventArgs e)
        {
            button_A.Enabled = false;
            button_B.Enabled = false;
            button_C.Enabled = false;
            button_D.Enabled = false;
            button_E.Enabled = false;
            button_F.Enabled = false;
            button_2.Enabled = false;
            button_3.Enabled = false;
            button_4.Enabled = false;
            button_5.Enabled = false;
            button_6.Enabled = false;
            button_7.Enabled = false;
            button_8.Enabled = false;
            button_9.Enabled = false;
            if (content.Checked) content.Text = label_bin.Text;
            if (content2.Checked) content2.Text = label_bin.Text;
            SetRadioColor();
            CheckForDot(label_bin.Text);
            radioButton_bin.ForeColor = Color.Black;
        }
        public void CheckForDot(string str)
        {
            char[] temp = str.ToCharArray();
            for (int i = 0; i < str.Length; i++)
            {
                if (temp[i] == '.')
                    dot = 1;
                else dot = 0;
            }
            if (dot == 0) button_dot.Enabled = true;
            else button_dot.Enabled = false;

        }
        public void SetRadioColor()
        {
            radioButton_bin.ForeColor = Color.FromArgb(255, 128, 0);
            radioButton_hex.ForeColor = Color.FromArgb(255, 128, 0);
            radioButton_dec.ForeColor = Color.FromArgb(255, 128, 0);
            radioButton_oct.ForeColor = Color.FromArgb(255, 128, 0);
        }
        private Systems DataCreate(string oper)
        {
            Systems sys=null;
            if (oper.Length  == 81)
            {
                MessageBox.Show("The string is too long! Last digits were deleted automatically!"
                    , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BackSpace();
            }
            if (oper == "-") goto end;
            try
            {
                if (radioButton_dec.Checked) sys = new Systems(oper);
                else if (radioButton_bin.Checked) sys = new Systems(oper, "Bin");
                else if (radioButton_oct.Checked) sys = new Systems(oper, "Oct");
                else if (radioButton_hex.Checked) sys = new Systems(oper, "Hex");
            }
            catch(System.OverflowException)
            {
                MessageBox.Show("Overflow! Last digits were deleted automatically! The result may be wrong",
                    "Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BackSpace();
                if (content.Checked) sys = DataCreate(content.Text);
                else sys = DataCreate(content2.Text);
            }
            end:;
            return sys;
        }
        private void InfOut(Systems sys)
        {
            if (sys != null)
            {
                label_bin.Text = sys.Bin;
                label_hex.Text = sys.Hex;
                label_oct.Text = sys.Oct;
                label_dec.Text = sys.Dec;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            button_A.Enabled = false;
            button_B.Enabled = false;
            button_C.Enabled = false;
            button_D.Enabled = false;
            button_E.Enabled = false;
            button_F.Enabled = false;
            button_2.Enabled = true;
            button_3.Enabled = true;
            button_4.Enabled = true;
            button_5.Enabled = true;
            button_6.Enabled = true;
            button_7.Enabled = true;
            button_8.Enabled = true;
            button_9.Enabled = true;
           content.Checked = true;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        }
        private void button_A_Click(object sender, EventArgs e)
        {
            Button click = sender as Button;
            if (content.Checked) 
            {
                content.Text += Entering(click);
                oper1 = DataCreate(content.Text);
                InfOut(oper1);
            }
            else if(content2.Checked)
            {
                content2.Text += Entering(click);
                oper2 = DataCreate(content2.Text);
                InfOut(oper2);
            }
            if (click.Text == "+")
            {
                content2.Checked = true;
                content.Checked = false;
                operation = "+";
                button_dot.Enabled = true;
            }
            else if (click.Text == "-")
            {
                content2.Checked = true;
                content.Checked = false;
                operation = "-";
                button_dot.Enabled = true;
            }
            else if (click.Text == "/")
            {
                content2.Checked = true;
                content.Checked = false;
                operation = "/";
                button_dot.Enabled = true;
            }
            else if (click.Text == "*")
            {
                content2.Checked = true;
                content.Checked = false;
                operation = "*";
                button_dot.Enabled = true;
            }
        }
        private string Entering(Button click)
        {
            string value=null;
            Regex reg = new Regex(@"button_([\dA-F])");
            Match m = reg.Match(click.Name);
            value = m.Groups[1].Value;
            return value;
        }
        public string Plus(Systems a, Systems b)
        {
            double temp=0;
            if (a != null || b != null)
            {
                temp = Convert.ToDouble(a.Dec) + Convert.ToDouble(b.Dec);
            }
            return temp.ToString();
        }
        public string Minus(Systems a, Systems b)
        {
            double temp = 0;
            if (a != null || b != null)
            {
                temp = Convert.ToDouble(a.Dec) - Convert.ToDouble(b.Dec);
            }
            return temp.ToString();
        }
        public string Mul(Systems a, Systems b)
        {
            double temp = 0;
            if (a != null || b != null) temp = Convert.ToDouble(a.Dec) * Convert.ToDouble(b.Dec);
            
            return temp.ToString();

        }
        public string Div(Systems a, Systems b)
        {
            double temp = 0;
            if (a != null || b != null) temp = Convert.ToDouble(a.Dec) / Convert.ToDouble(b.Dec);
            return temp.ToString();
        }
        private void button_equal_Click(object sender, EventArgs e)
        {
            string data="";
            if (content.Text == ""||content2.Text=="")
            {
                MessageBox.Show("Empty operand!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                content2.Text = "";
                content.Text = "";
            }
            else
            {
                if (operation == "+") data = Plus(oper1, oper2);
                else if (operation == "-") data = Minus(oper1, oper2);
                else if (operation == "*") data = Mul(oper1, oper2);
                else if (operation == "/")
                {
                    if (Convert.ToInt32(content2.Text) == 0)
                    {
                        MessageBox.Show("Division by 0!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        content.Checked = true;
                        content2.Checked = false;
                        content2.Text = null;
                        goto mark;
                    }
                    else data = Div(oper1, oper2);
                }
            }
                content.Checked = true;
                content2.Text = null;
                content2.Checked = false;
                radioButton_dec.Checked = true;
                content.Text = data;
                res = DataCreate(data);
                InfOut(res);
                mark:
                operation = null;
        }
        int clearcnt = 1;
        private void button_ms_Click(object sender, EventArgs e)
        {
            Button click1 = sender as Button;
            string name = null;
            if (radioButton_bin.Checked) name = "Bin";
            else if (radioButton_oct.Checked) name = "Oct";
            else if (radioButton_dec.Checked) name = "Dec";
            else if (radioButton_hex.Checked) name = "Hex";
            if (click1.Text == "MS")
            {
                if (content.Checked && content.Text != "") NewMemory(content.Text, name);
                else if (content2.Checked && content2.Text != "") NewMemory(content2.Text, name);
            }
            else if (click1.Text == "MC") clear();
            else if (click1.Text == "M+")
            {
                if (content.Checked && content.Text!="") add(content.Text, name);
                else if (content2.Checked && content2.Text != "") add(content2.Text, name);
            }
            else if (click1.Text == "M-")
            {
                if (content.Checked && content.Text != "") minus(content.Text, name);
                else if (content2.Checked && content2.Text != "") minus(content2.Text, name);
            }
        }
        //////MEMORY METHODS///////     
        struct data
        {
            public string val;
            public string name;
        };
        LinkedList<data> value = new LinkedList<data>();
        Systems op1;
        Systems op2;
        public void NewMemory(string text, string name)
        {
            data temp = new data();
            temp.val = text;
            temp.name = name;
            value.AddFirst(temp);
            listBox_mem.Items.Insert(0, temp.val);
            clearcnt = 0;
        }
        public void clear()
        {
            for (int j = 0; j < listBox_mem.Items.Count; j++)
            {
               value.RemoveFirst();
            }
            listBox_mem.Items.Clear();
            listBox_mem.ClearSelected();
            clearcnt = 1;
        }
        public void add(string val, string name)
        {
            if (clearcnt == 0)
            {
                try
                {
                    data temp = value.ElementAt<data>(0);
                    op1 = DataCreateMemory(val, name);
                    op2 = DataCreateMemory(temp.val, temp.name);
                    string temp1 = Plus(op1, op2);
                    Systems tmp = DataCreateMemory(temp1, "Dec");
                    if (temp.name == "Dec") listBox_mem.Items[0] = tmp.Dec;
                    else if (temp.name == "Bin") listBox_mem.Items[0] = tmp.Bin;
                    else if (temp.name == "Oct") listBox_mem.Items[0] = tmp.Oct;
                    else if (temp.name == "Hex") listBox_mem.Items[0] = tmp.Hex;
                    temp.val = listBox_mem.Items[0].ToString();
                    value.RemoveFirst();
                    value.AddFirst(temp);
                }catch(OverflowException)
                {
                    MessageBox.Show("Overflow!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void minus(string val, string name)
        {
            if (clearcnt == 0)
            {
                try
                {
                    data temp = value.ElementAt<data>(0);
                    op1 = DataCreateMemory(val, name);
                    op2 = DataCreateMemory(temp.val, temp.name);
                    string temp1 = Minus(op2, op1);
                    Systems tmp = DataCreateMemory(temp1, "Dec");
                    if (temp.name == "Dec") listBox_mem.Items[0] = tmp.Dec;
                    else if (temp.name == "Bin") listBox_mem.Items[0] = tmp.Bin;
                    else if (temp.name == "Oct") listBox_mem.Items[0] = tmp.Oct;
                    else if (temp.name == "Hex") listBox_mem.Items[0] = tmp.Hex;
                    temp.val = listBox_mem.Items[0].ToString();
                    value.RemoveFirst();
                    value.AddFirst(temp);
                }catch(OverflowException)
                {
                    MessageBox.Show("Overflow!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public Systems DataCreateMemory(string oper, string name)
        {
            Systems sys = null;
            if (name == "Dec") sys = new Systems(oper);
            else sys = new Systems(oper, name);
            return sys;
        }

        private void listBox_mem_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Systems sys = null;
                int ind = listBox_mem.SelectedIndex;
                data temp = value.ElementAt<data>(ind);
                if (temp.name == "Dec") radioButton_dec.Checked = true;
                else if (temp.name == "Bin") radioButton_bin.Checked = true;
                else if (temp.name == "Oct") radioButton_oct.Checked = true;
                else if (temp.name == "Hex") radioButton_hex.Checked = true;
                if (content.Checked)
                {
                    content.Text = temp.val;
                    sys = DataCreate(content.Text);
                }
                else
                {
                    content2.Text = temp.val;
                    sys = DataCreate(content2.Text);
                }
                InfOut(sys);
                listBox_mem.ClearSelected();
            }catch
            {
                MessageBox.Show("Memory is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                goto mark;
            }
            mark:;
        }

        private void button_dot_Click(object sender, EventArgs e)
        {
            if (content.Checked && content.Text != "" && dot == 0) 
            {
                content.Text += ".";
                button_dot.Enabled = false;
                dot = 1;
            }
            else if (content2.Checked && content2.Text != "" && dot == 0)
            {
                content2.Text += ".";
                button_dot.Enabled = false;
                dot = 1;
            }
        }

        private void content2_Click(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked) ((CheckBox)sender).Checked = false;
            else ((CheckBox)sender).Checked = true;
            CheckForDot(content.Text);
        }

        private void content_Click(object sender, EventArgs e)
        {
            if (content.Checked) content.Checked = true;
            else if (!content.Checked) content.Checked = false;
            CheckForDot(content2.Text);
        }
        int dot = 0;
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char tmp = e.KeyChar;
            if (radioButton_bin.Checked)
            {
                if (tmp == '0' || tmp == '1') Key(tmp);
                else if (tmp == '.')
                    if (content.Text != ""&& dot==0) Key(tmp);
            }
            else if(radioButton_dec.Checked)
            {
                if (tmp >= '0' && tmp <= '9') Key(tmp);
                else if (tmp == '.')
                    if (content.Text != "" && dot == 0) Key(tmp);
            }
            else if (radioButton_oct.Checked)
            {
                if (tmp >= '0' && tmp <= '7') Key(tmp);
                else if (tmp == '.')
                    if (content.Text != "" && dot == 0) Key(tmp);
            }
            else if (radioButton_hex.Checked)
            {
                if ((tmp >= '0' && tmp <= '9') || (tmp >= 'A' && tmp <= 'F'))
                    Key(tmp);
                else if (tmp == '.')
                    if (content.Text != "" && dot == 0) Key(tmp);
            } 
        }
        public void Key(char a)
        {
            if (content.Checked)
            {
                content.Text += a;
                oper1 = DataCreate(content.Text);
                InfOut(oper1);
            }
            else
            {
                content2.Text += a;
                oper2 = DataCreate(content2.Text);
                InfOut(oper2);
            }
            if (a == '.') dot++; 
        }
        private void content2_CheckedChanged(object sender, EventArgs e)
        {
            dot = 0;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Back)
            {
                e.SuppressKeyPress = true;
                BackSpace();
            }
        }
        public void BackSpace()
        {
            char[] str;
            if(content.Checked)
            {
                str = content.Text.ToCharArray();
                content.Text = null;
                for (int i = 0; i < str.Length - 1; i++)
                {
                    content.Text += str[i].ToString();
                    if (str[i] == '.') dot = 1;
                    else
                    {
                        dot = 0;
                        button_dot.Enabled = true;
                    }
                }
                oper1 = DataCreate(content.Text);
                InfOut(oper1);
            }
            else if(content2.Checked)
            {
                str = content2.Text.ToCharArray();
                content2.Text = null;
                for (int i = 0; i < str.Length - 1; i++)
                {
                    content2.Text += str[i].ToString();
                    if (str[i] == '.') dot = 1;
                    else
                    {
                        dot = 0;
                        button_dot.Enabled = true;
                    }
                }
                    oper2 = DataCreate(content2.Text);
                    InfOut(oper2);
            }
            
        }

        private void Back_Click(object sender, EventArgs e)
        {
            BackSpace();
        }

        private void AC_Click(object sender, EventArgs e)
        {
            content.Text = "";
            content.Checked = true;
            content2.Checked = false;
            content2.Text = "";
            oper1 = DataCreate(content.Text);
            InfOut(oper1);
        }
    }

}