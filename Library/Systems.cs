﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Library
{
    public class Systems
    {
        public string Hex { private set; get; }
        public string Dec { private set; get; }
        public string Oct { private set; get; }
        public string Bin { private set; get; }
        public Systems(string dec)
        {
            Hex = DecOther(dec,"Hex");
            Dec = dec;
            Oct = DecOther(dec,"Oct");
            Bin = DecOther(dec,"Bin");
        }
        public Systems(string num, string syst)
        {
            Hex = DecOther(OtherDec(num, syst),"Hex");
            Dec = OtherDec(num,syst);
            Oct = DecOther(OtherDec(num, syst),"Oct");
            Bin = DecOther(OtherDec(num, syst), "Bin");
        }
        public static string DecOther(string str, string nameres)
        {
            if (str == "") return "";
            int div = 0;
            if (nameres == "Bin") div = 2;
            else if (nameres == "Oct") div = 8;
            else if (nameres == "Hex") div = 16;
            bool negative = false, dot = false;
            string res = null;
            char[] temp1 = str.ToCharArray();
            for (int i = 0; i < str.Length; i++)
            {
                if (temp1[i] == '.')
                {
                    dot = true; break;
                }
                res += temp1[i];
            }
            int temp = Convert.ToInt32(res);
            res = null;
            if (temp < 0)
            {
                temp = -temp;
                temp.ToString();
                negative = true;
            }
            bool var = true;
            while (var)
            {
                if (temp % div > 9)
                {
                    char z = 'A';
                    for (int i = 10; i < temp % 16; i++) z++;
                    res += z.ToString();
                    temp /= 16;
                }
                else
                {
                    res += temp % div;
                    temp /= div;
                }
                if (temp == 0) var = false;
            }
            res = Reverce(res);
            if (negative) res = Inverse(res, nameres);
            if (dot) res+=DecOtherDot(str,nameres);
            return res;
        }
        public static string OtherDec(string str, string namein)
        {
            if (str == "") return "";
            string drob=null;
            double dec = 0;
            int mul = 0;
            int drobcnt=0;
            if (namein == "Bin") mul = 2;
            else if (namein == "Oct") mul = 8;
            else if (namein == "Hex") mul = 16;
            char[] temp = str.ToCharArray();
            str = null;
            for(int i=0;i<temp.Length;i++)
            {
                if (temp[i] == '.') drobcnt++;
                if (temp[i] == '.' && i != temp.Length - 1) 
                { 
                    for (int j = i + 1; j < temp.Length; j++)
                    {
                        drobcnt++;
                        str += temp[j];
                    }
                    drob = OtherDecDot(str, mul);
                    break;
                }
            }
            int step=temp.Length-drobcnt-1;
            int n = step;
            for (int i = 0; i <= n; i++)
            {
                if (temp[i] == '.') break;
                else if (temp[i] >= 'A')
                {
                    dec += (Convert.ToDouble(Convert.ToInt32(temp[i])) - 55) * Math.Pow(mul, step);
                }
                else dec += Convert.ToDouble(temp[i].ToString()) * Math.Pow(mul, step);
                step--;
            }
            Convert.ToInt32(dec);
            return dec.ToString()+drob;
        }
        public static string OtherDecDot(string str,int mul)
        {
            char[] temp = str.ToCharArray();
            int step = -1;
            double res = 0;
            for(int i=0;i<temp.Length;i++)
            {
                if (temp[i] >= 'A')
                {
                    res += (Convert.ToDouble(Convert.ToInt32(temp[i])) - 55) * Math.Pow(mul, step);
                }
                else res += Convert.ToDouble(temp[i].ToString()) * Math.Pow(mul, step);
                step--;
            }
            temp = res.ToString().ToCharArray();
            str = null;
            for (int i = 1; i < temp.Length; i++) str += temp[i];
            return str;
        }
        public static string Reverce(string str)
        {
            return new string(str.ToCharArray().Reverse().ToArray());
        }
        public static string DecOtherDot(string str, string nameres)
        {
            string res = null;
            double mul = 0;
            if (nameres == "Bin") mul = 2;
            else if (nameres == "Oct") mul = 8;
            else if (nameres == "Hex") mul = 16;
            char[] temp = str.ToCharArray();
            int n = str.Length;
            int step = 0;
            str = null;
            for (int i = 0; i < n; i++)
                if (temp[i] == '.')
                {
                    for (int j = i + 1; j < n; j++)
                        str += temp[j];
                    step = n - (i + 1);
                }
            int zerocnt = 0;
            double tmp = Convert.ToDouble(str) / Math.Pow(10, step);
            char[] overr;
            for (int i = 0; i < 5; i++)
            {
                string over = null;
                tmp *= mul;
                if (tmp >= 1)
                {
                    res += SettingRes(tmp);
                    zerocnt = 0;
                }
                else
                {
                    res += "0";
                    zerocnt++;
                }
                 overr = tmp.ToString().ToCharArray();
                for (int j = 0; j < overr.Length; j++)
                    if (overr[j] == '.')
                    {
                        for (int z = j + 1; z < overr.Length; z++)
                            over += overr[z];
                        step = overr.Length - (j + 1);
                    }
                tmp = Convert.ToDouble(over) / Math.Pow(10, step);
            }
            overr = res.ToCharArray();
            if (zerocnt != 0)
            {
                res = null;
                for (int i = 0; i < overr.Length - zerocnt; i++)
                    res += overr[i].ToString();
            }
            if (zerocnt == 5) res += "0";
            return "." + res;
        }
        public static string SettingRes(double tmp)
        {
            string res = null;
            char[] overr = tmp.ToString().ToCharArray();
            for (int i = 0; i < overr.Length; i++)
            {
                if (overr[i] == '.') break;
                res += overr[i];
            }
            int temp = Convert.ToInt32(res);
            res = null;
            if (temp > 9)
            {
                char z = 'A';
                for (int i = 10; i < temp; i++) z++;
                res += z.ToString();
            }
            else res += temp.ToString();
            return res;
        }
        public static string Inverse(string str, string nameres)
        {
            str = OtherDec(str, nameres);
            str = DecOther(str, "Bin");
            int n = str.Length;
            if (n <= 8) n = 8;
            else if (n <= 16) n = 16;
            else n = 32;
            str = Reverce(str);
            for (int i = str.Length; i < n; i++) str += "0";
            char[] z = str.ToCharArray();
            Array.Reverse(z);
            n = str.Length;
            str = null;
            for (int i = 0; i < n; i++)
            {
                if (z[i] == '1') z[i] = '0';
                else if (z[i] == '0') z[i] = '1';
                str += z[i];
            }
            string res = OtherDec(str, "Bin");
            int tmp = Convert.ToInt32(res);
            tmp = tmp + 1;
            res = tmp.ToString();
            res = DecOther(res, nameres);
            return res;
        }
    }
}
